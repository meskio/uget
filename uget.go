package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"time"

	utls "github.com/refraction-networking/utls"
	"golang.org/x/net/http2"
)

var (
	clientHelloIDMap = map[string]*utls.ClientHelloID{
		"hellogolang":           nil, // Don't bother with utls.
		"hellorandomized":       &utls.HelloRandomized,
		"hellorandomizedalpn":   &utls.HelloRandomizedALPN,
		"hellorandomizednoalpn": &utls.HelloRandomizedNoALPN,
		"hellofirefox_auto":     &utls.HelloFirefox_Auto,
		"hellofirefox_55":       &utls.HelloFirefox_55,
		"hellofirefox_56":       &utls.HelloFirefox_56,
		"hellofirefox_63":       &utls.HelloFirefox_63,
		"hellofirefox_65":       &utls.HelloFirefox_65,
		"hellofirefox_99":       &utls.HelloFirefox_99,
		"hellofirefox_102":      &utls.HelloFirefox_102,
		"hellofirefox_105":      &utls.HelloFirefox_105,
		"hellochrome_auto":      &utls.HelloChrome_Auto,
		"hellochrome_58":        &utls.HelloChrome_58,
		"hellochrome_62":        &utls.HelloChrome_62,
		"hellochrome_70":        &utls.HelloChrome_70,
		"hellochrome_72":        &utls.HelloChrome_72,
		"hellochrome_83":        &utls.HelloChrome_83,
		"hellochrome_87":        &utls.HelloChrome_87,
		"hellochrome_96":        &utls.HelloChrome_96,
		"hellochrome_100":       &utls.HelloChrome_100,
		"hellochrome_102":       &utls.HelloChrome_102,
		"helloios_auto":         &utls.HelloIOS_Auto,
		"helloios_11_1":         &utls.HelloIOS_11_1,
		"helloios_12_1":         &utls.HelloIOS_12_1,
		"helloios_13":           &utls.HelloIOS_13,
		"helloios_14":           &utls.HelloIOS_14,
		"helloandroid_11":       &utls.HelloAndroid_11_OkHttp,
		"helloedge_auto":        &utls.HelloEdge_Auto,
		"helloedge_85":          &utls.HelloEdge_85,
		"helloedge_106":         &utls.HelloEdge_106,
		"hellosafari_auto":      &utls.HelloSafari_Auto,
		"hellosafari_16_0":      &utls.HelloSafari_16_0,
		"hello360_auto":         &utls.Hello360_Auto,
		"hello360_7_5":          &utls.Hello360_7_5,
		"hello360_11_0":         &utls.Hello360_11_0,
		"helloqq_auto":          &utls.HelloQQ_Auto,
		"helloqq_11_1":          &utls.HelloQQ_11_1,
	}
	defaultClientHello = &utls.HelloFirefox_Auto

	dialTimeout = time.Duration(15) * time.Second
)

func HttpGet(uri *url.URL, clientHello *utls.ClientHelloID) ([]byte, error) {
	rawConn, err := net.Dial("tcp", uri.Host+":443")
	if err != nil {
		return nil, err
	}
	tlsConn := utls.UClient(rawConn, &utls.Config{
		ServerName: uri.Host,

		// `crypto/tls` gradually ramps up the record size.  While this is
		// a good optimization and is a relatively common server feature,
		// neither Firefox nor Chromium appear to use such optimizations.
		DynamicRecordSizingDisabled: true,
	}, *clientHello)
	defer tlsConn.Close()

	if err = tlsConn.Handshake(); err != nil {
		log.Println("http handshake error")
		return nil, err
	}

	resp, err := httpGetOverConn(tlsConn, tlsConn.ConnectionState().NegotiatedProtocol, uri)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(resp.Body)
}

func httpGetOverConn(conn net.Conn, alpn string, uri *url.URL) (*http.Response, error) {
	req := &http.Request{
		Method: "GET",
		URL:    uri,
		Header: make(http.Header),
		Host:   uri.Host,
	}

	switch alpn {
	case "h2":
		req.Proto = "HTTP/2.0"
		req.ProtoMajor = 2
		req.ProtoMinor = 0

		tr := http2.Transport{}
		cConn, err := tr.NewClientConn(conn)
		if err != nil {
			return nil, err
		}
		return cConn.RoundTrip(req)
	case "http/1.1", "":
		req.Proto = "HTTP/1.1"
		req.ProtoMajor = 1
		req.ProtoMinor = 1

		err := req.Write(conn)
		if err != nil {
			return nil, err
		}
		return http.ReadResponse(bufio.NewReader(conn), req)
	default:
		return nil, fmt.Errorf("unsupported ALPN: %v", alpn)
	}
}

func main() {
	var fp string
	flag.StringVar(&fp, "fp", "", "Fingerprint to use for the get")
	flag.Parse()
	if flag.NArg() == 0 {
		log.Fatal("provide a url")
	}
	urlStr := flag.Arg(0)

	clientHello := defaultClientHello
	if fp != "" {
		clientHello = clientHelloIDMap[fp]
		if clientHello == nil {
			log.Fatal("Invalid fingerprint ", fp)
		}
	}

	uri, err := url.Parse(urlStr)
	if err != nil {
		log.Fatalf("Can't parse %s: %v", urlStr, err)
	}
	body, err := HttpGet(uri, clientHello)
	if err != nil {
		log.Fatalf("Can't get %s: %v", urlStr, err)
	}

	fmt.Println(string(body))
}
